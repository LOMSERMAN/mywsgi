from genericpath import isfile
from wsgiref import simple_server
from application import application


def static_middleware(app):
    """
    Static files dispenser. Only Python, only hardcode. #flexibilityisforpussies
    """
    def wrapper(environ, start_response):
        path = environ.get('PATH_INFO', "")
        headers = []
        if path.startswith("/static"):
            path = path[1:]     # Remove first '/'
            if not isfile(path):
                body = "ERROR 404: Static not found :<"
                status = "404 NOT FOUND"
                headers.append(("Content-Type", "text/plain"))
            else:
                with open(path, encoding="utf-8") as s:
                    body = s.read()
                status = "200 OK"
                if path.endswith(".css"):
                    headers.append(("Content-Type", "text/css"))
                elif path.endswith(".js"):
                    headers.append(("Content-Type", "text/javascript"))
                else:
                    headers.append(("Content-Type", "text/plain"))
            headers.append(("Content-Length", str(len(body))))
            start_response(status, headers)
            return [body.encode()]
        else:
            return app(environ, start_response)
    return wrapper

# Требования и ограничения: приложение должно запускаться на любом web сервере поддерживающем wsgi стандарт. OKAY.
httpd = simple_server.make_server("localhost", 80, static_middleware(application))
httpd.serve_forever()

