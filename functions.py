# Different common code, models and functions, not related to my makeshift "Framework" and views.
from framework import RegexpValidator, get_cursor, get_connection

name_validator = RegexpValidator("^[a-zA-Zа-яА-Я\-]{3,30}$",
                                 "Only combination of letters and symbol '-' are allowed, 3-30 characters.")
phone_validator = RegexpValidator("^\+?\d{1,4}\(\d{3,6}\)[\d\-]{5,10}$", "Incorrect phone number")
email_validator = RegexpValidator('^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]'
                                  '{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$',
                                  "Invalid email address.")
comment_validator = RegexpValidator("^.{3,300}$", "Comment must have length 3 to 300 symbols.")
id_validator = RegexpValidator("^\d+$", "Incorrect id. Did you forget to choose item in the list?")


class CommentModel:
    fields = ["id", "name", "middle_name", "surname", "city_id", "region_id", "phone", "email", "comment"]

    def __init__(self):
        self.cursor = get_cursor()
        self.conn = get_connection()

    def add(self, name, middle_name, surname, city, region, phone, email, comment):
        self.cursor.execute(
            "INSERT INTO comments(name, middle_name, surname, city_id, region_id, phone, email, comment) "
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)", (name, middle_name, surname, city, region, phone, email,
                                                comment))
        self.conn.commit()

    def delete(self, id):
        self.cursor.execute("DELETE FROM comments WHERE id=?", (id,))
        self.conn.commit()

    def exists(self, id):
        self.cursor.execute("SELECT FROM comments WHERE id=?", (id,))
        return self.cursor.fetchone() is not None

    def fetch_everything(self):
        result = []
        self.cursor.execute("SELECT %s FROM comments ORDER BY id" % ", ".join(self.fields))
        for comment_data in self.cursor.fetchall():
            result.append(dict(zip(self.fields, comment_data)))
        print(result)
        return result

    def fetch_all(self, sql, params=()):
        self.cursor.execute(sql, params)
        return self.cursor.fetchall()

    def fetch_one(self, sql, params=()):
        self.cursor.execute(sql, params)
        return self.cursor.fetchone()
