import json

from framework import render_to_response, JsonResponse, Error400, parse_json_request, simple_screen
from functions import *



def index(request):
    return render_to_response(request, "templates/index.html")


def add_comment(request):
    if request.method != "GET":
        raise Error400("Unsupported request method %s" % request.method)
    cursor = get_cursor()
    cursor.execute("SELECT id, name FROM regions")
    fetched = cursor.fetchall()
    regions = {}
    for region in fetched:
        regions[region[0]] = region[1]
    return render_to_response(request, "templates/add_comment.html", {
        "regions": json.dumps(regions),
    })


def view_comments(request):
    comments = CommentModel().fetch_everything()
    return render_to_response(request, "templates/view_comments.html", {
        "comments": simple_screen(json.dumps(comments)),
    })


def stat_common(request):
    statistics = CommentModel().fetch_all("SELECT regions.name, comments.region_id, count(comments.id) "
                                          "FROM comments "
                                          "LEFT JOIN regions ON comments.region_id = regions.id "
                                          "GROUP BY region_id "
                                          "HAVING count(comments.id) > 5")
    result = []
    for row in statistics:
        result.append(dict(zip(("region_name", "region_id", "comments_count"), row)))
    return render_to_response(request, "templates/stat_common.html", {
        "statistics": json.dumps(result),
    })


def stat_region(request, region_id):
    statistics = CommentModel().fetch_all("SELECT cities.name, count(comments.id) as comment_count FROM comments "
                                          "LEFT JOIN cities ON comments.city_id = cities.id "
                                          "WHERE comments.region_id = %s GROUP BY city_id" % region_id)

    result = []
    for row in statistics:
        result.append(dict(zip(("city", "comments_count"), row)))

    region_name = CommentModel().fetch_one("SELECT name from regions where id = ?", (region_id,))[0]

    return render_to_response(request, "templates/stat_region.html", {
        "statistics": json.dumps(result),
        "region_name": region_name
    })


# todo: check if region exists.
def api_cities(request):
    cursor = get_cursor()
    if request.method == "POST":
        json_request = parse_json_request(request)
        region_id = json_request['region_id']
        print("Received request with region_id = %s" % region_id)
        cursor.execute("SELECT id, name FROM cities WHERE region_id = ?", (region_id,))
        fetched = cursor.fetchall()
        cities = {}
        for city in fetched:
            cities[city[0]] = city[1]
        return JsonResponse(request, {"cities": cities})
    else:
        raise Error400("Bad request for api_cities")


def api_regions(request):
    pass


def api_comments(request):
    errors = {}

    def get_and_validate(form_dict, field_id, validator=None, required=False, default=None):
        if field_id not in form_dict or len(form_dict[field_id]) == 0:
            if required:
                errors[field_id] = "Field not found in request."
                return
            else:
                return default
        if validator is not None:
            validation_result = validator.validate(form_dict[field_id])
            if validation_result != 0:
                errors[field_id] = validation_result
        return form_dict[field_id]

    if request.method == "POST":
        form = parse_json_request(request)
        action = get_and_validate(form, "action")
        if action == "ADD":
            name = get_and_validate(form, "name", name_validator, True)
            surname = get_and_validate(form, "surname", name_validator, True)
            middle_name = get_and_validate(form, "middle_name", name_validator)
            phone = get_and_validate(form, "phone", phone_validator)
            email = get_and_validate(form, "email", email_validator)
            region = get_and_validate(form, "region", id_validator)
            city = get_and_validate(form, "city", id_validator)
            comment = get_and_validate(form, "comment", comment_validator, True)
            print("Attempt to add %s" % form)
            if len(errors) > 0:
                print("ERROR: comment invalid data")
                return JsonResponse(request, errors={'field_errors': errors}, response_code=400)
            CommentModel().add(name, middle_name, surname, city, region, phone, email, comment)
            return JsonResponse(request)
        if action == "DEL":
            comment_id = get_and_validate(form, "id")
            print("comment_id  %s" % comment_id)
            CommentModel().delete(comment_id)
            return JsonResponse(request)
        raise Error400("Unsupported 'action' param %s" % action)

    raise Error400("Unsupported request method %s" % request.method)
