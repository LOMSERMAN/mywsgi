CREATE TABLE IF NOT EXISTS regions (
  id INTEGER PRIMARY KEY,
  name TEXT
);

CREATE TABLE IF NOT EXISTS cities (
  id INTEGER PRIMARY KEY,
  name TEXT,
  region_id INTEGER,
  FOREIGN KEY (region_id) REFERENCES regions(id)
);

CREATE TABLE IF NOT EXISTS comments (
  id INTEGER PRIMARY KEY,
  name TEXT,
  middle_name TEXT,
  surname TEXT,
  city_id INTEGER,
  region_id INTEGER,
  phone TEXT,
  email TEXT,
  comment TEXT,
  FOREIGN KEY (city_id) REFERENCES cities(id),
  FOREIGN KEY (region_id) REFERENCES regions(id)
);

INSERT INTO regions(name) VALUES
('Краснодарский край'),
('Ростовская область'),
('Ставропольский край');

INSERT INTO cities(name, region_id)
SELECT cities.'name', regions.id
FROM regions,
  (SELECT 'Краснодар' as name UNION
   SELECT 'Кропоткин' UNION
   SELECT 'Славянск') as cities
WHERE regions.name='Краснодарский край';

INSERT INTO cities(name, region_id)
SELECT cities.'name', regions.id
FROM regions,
  (SELECT 'Ростов' as name UNION
   SELECT 'Шахты' UNION
   SELECT 'Батайск') as cities
WHERE regions.name='Ростовская область';

INSERT INTO cities(name, region_id)
SELECT cities.'name', regions.id
FROM regions,
  (SELECT 'Ставрополь' as name UNION
   SELECT 'Пятигорск' UNION
   SELECT 'Кисловодск') as cities
WHERE regions.name='Ставропольский край';