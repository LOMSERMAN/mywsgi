// Project-wide garage with bicycles, because js-frameworks are forbidden.

// Monkey patch for NodeList.
NodeList.prototype.forEach = Array.prototype.forEach;

// Monkey patch for String.
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

// fake jquery :)
var $ = function (selector) {
    return document.querySelector(selector);
};

// shortcut
function ready(func) {
    document.addEventListener('DOMContentLoaded', func);
}

// shortcut
function regex_validate(selector, pattern) {
    var data = $(selector).value;
    return data.search(pattern) >= 0;
}

function clear_select(select) {
    while (select.options.length > 0) {
        select.remove(0);
    }
}

function fill_select(select, options) {
    for (var key in options) {
        var opt = document.createElement("option");
        opt.value = key;
        select.appendChild(opt);
        opt.innerHTML = options[key];
        console.log(key + " " + options[key]);
    }
}


// in case if we can't parse server's response, we still need to provide our functions with valid jsons.
var error_cant_parse = {
    response: {},
    info: [],
    errors: {error: "Cannot parse server's response"}
};

function parseJSONOrFallback(response) {
    try {
        return JSON.parse(response);
    } catch(e) {
        console.log("ERROR: invalid json, cannot be parsed - " + response);
        return error_cant_parse;
    }
}

function ajaxPostJson(options) {
    var address = options.address || "";
    var json = options.json || {};
    var success = options.success;
    var fail = options.fail;

    var xhr = new XMLHttpRequest();
    xhr.open("POST", address, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(encodeURIComponent(JSON.stringify(json)));
    xhr.onload = function () {
        var response = parseJSONOrFallback(xhr.responseText);
        if (xhr.status == 200) {
            console.log("ajaxPostJson Success!");
            success(xhr.status, response);
        }
        else {
            console.log("ajaxPostJson fail!");
            fail(xhr.status, response);
        }
    };
}


// Bunch of shortcuts for info and error display.
// We assume that page will have <ul> elements with corresponding "errors" and "infos" ids.
function add_li(ul, text) {
    var li = document.createElement("li");
    li.innerHTML = text;
    ul.appendChild(li);
}

function clear_ul(ul) {
    ul.innerHTML = "";
}

function addError(text) {
    add_li($("#errors"), text);
}

function clearErrors() {
    clear_ul($("#errors"));
}

function addInfo(text) {
    add_li($("#infos"), text);
}

function clearInfos() {
    clear_ul($("#infos"));
}

/*
Simple regexp validator for strings. validate() returns 0 if valid, otherwise return errorText.
 */
function Validator(regExp, errorText) {
    this.regExp = regExp;
    this.errorText = errorText;
    this.validate = function (field) {
        var value = field.value;
        if (field.required && (typeof value != "string" || value.length == 0))
            return "this field is required";
        if (value.length > 0 && value.search(this.regExp) < 0) return this.errorText;
        return 0;
    }
}


// shortcut to get rid of 'null' strings fast.
function nenull(str) {
    if (str == null)
        return "";
    return str;
}

/*
Primitive template mechanism.
 */
function SimpleTemplate(templateText) {
    this.templateText = templateText;

    /**
     * Replaces all <%variables%> from original templateText by corresponding var from variables param.
     * @param variables - json
     * @returns {*}
     */
    this.render = function (variables) {
        var result = this.templateText;
        for (var variable in variables) {
            var replacement = nenull(variables[variable]);
            result = result.replaceAll("<%" + variable + "%>", replacement);
            result = result.replaceAll("&lt;%" + variable + "%&gt;", replacement);
        }
        return result;
    }
}
