from framework import Url, Error404, Request, url_dispatcher, return_404, init_db_cursor, return_500, Error500, Error400, \
    return_400
from views import *

urls = [
    Url(r"^/comment/$", add_comment),
    Url(r"^/view/$", view_comments),
    Url(r"^/stat/$", stat_common),
    Url(r"^/stat/(\d+)/$", stat_region),
    Url(r"^/$", index),
    Url(r"/api/cities/$", api_cities),
    Url(r"/api/regions/$", api_regions),
    Url(r"/api/comments/$", api_comments)
]


def application(environ, start_response):
    print(environ)
    request = Request(environ, start_response)
    init_db_cursor("my.db", "init.sql")
    # noinspection PyBroadException
    try:
        response = url_dispatcher(urls, request)
        if response is None:
            raise Error500("Url dispatcher returned None instead of response.")
    except Error400:
        return return_400(request)
    except Error404:
        return return_404(request)
    except Exception:              # For any other unexpected exceptions.
        return return_500(request)
    return response.render()
