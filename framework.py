import json
import os
import re
import sqlite3
import traceback
from copy import deepcopy
import urllib
from urllib.parse import unquote


class HttpError(Exception):
    pass


class Error400(HttpError):
    pass


class Error404(HttpError):
    pass


class Error500(HttpError):
    pass


# True stateless database connection :)
_cursor = None
_connection = None


def assert_types(variable, list_of_classes, varname):
    """
    Assert if variable is an instance of any class from the list (None is acceptable too).
    :param variable:
    :param list_of_classes:
    :param varname: Needed only for Exception's text. Simplifies debugging.
    :return:
    """
    for klass in list_of_classes:
        if klass is None and variable is None:
            return
        if isinstance(variable, klass):
            return
    raise AssertionError("Variable %s must be instance of %s, got %s instead" % (varname, list_of_classes,
                                                                                 type(variable)))


def init_db_cursor(db_name, sql_file):
    """
    Creates database cursor if cursor doesn't exist yet.
    Also creates and init db if database's file doesn't exist yet.
    :param db_name: db file name.
    :param sql_file: file with init scripts.
    :return:
    """
    global _cursor, _connection
    if _cursor is not None:
        return
    init = False
    if not os.path.isfile(db_name):
        init = True
    _connection = sqlite3.connect(db_name)
    _connection.text_factory = str
    _cursor = _connection.cursor()
    if init:
        with open(sql_file, encoding="UTF-8") as f:
            sql = f.read()
        _cursor.executescript(sql)


def get_connection():
    if _connection is None:
        raise Exception("DB connection doesn't exist")
    return _connection


def get_cursor():
    if _cursor is None:
        raise Exception("DB cursor isn't open")
    return _cursor


class Request(dict):
    def __init__(self, environ, start_response, **kwargs):
        super().__init__(**kwargs)
        self.wsgi_handler = start_response
        self.environ = environ
        self.method = environ.get('REQUEST_METHOD', "").upper()
        self.path = environ.get('PATH_INFO', '/')
        self.isJSON = ('CONTENT_TYPE' in environ and environ['CONTENT_TYPE'].startswith('application/json'))

        if self.method == "POST":
            body_size = int(environ.get('CONTENT_LENGTH', 0))
            self.request_body = environ['wsgi.input'].read(body_size)


def url_dispatcher(urls, request):
    for url in urls:
        match = url.match(request.path)
        if match is not None:
            kwargs = match.groupdict()
            args = () if kwargs else match.groups()
            return url.callback(request, *args, **kwargs)
    raise Error404("PATH '%s' not found in urls" % request.path)


class Url:
    def __init__(self, pattern, callback):
        self.raw_pattern = pattern
        self.pattern = re.compile(pattern)
        self.callback = callback

    def match(self, address):
        return self.pattern.match(address)


class Response:
    def __init__(self, request, content=None, response_code=200):
        self._headers = {}
        self._status = ""
        self.set_status(response_code)
        self._wsgi_handler = request.wsgi_handler
        self.content = content or ""
        # self.add_header("Access-Control-Accept-Headers", "*")

    def render(self):
        self._wsgi_handler(self._status, self._compiled_headers())
        return [self.content.encode()]

    def add_header(self, name, value):
        self._headers[name] = value

    def del_header(self, name):
        del self._headers[name]

    def set_status(self, code):
        if code == 200:
            self._status = "200 OK"
        elif code == 400:
            self._status = "400 BAD REQUEST"
        elif code == 404:
            self._status = "404 NOT FOUND"
        elif code == 500:
            self._status = "500 INTERNAL SERVER ERROR"
        else:
            raise Exception("Status '%s' isn't supported yet, sorry :'( P.S.: PLS SEND MORE MONEY" % code)

    def _compiled_headers(self):
        headers = []
        for k, v in self._headers.items():
            headers.append((k, v))
        headers.append(("Content-Length", str(len(self.content))))
        return headers


class JsonResponse(Response):
    """
    Standardized JSON reply. A simple way not to confuse myself.
    """
    def __init__(self, request: Request, response=None, errors=None, info=None, response_code=200):
        assert_types(response, [dict, None], 'response')
        assert_types(errors, [dict, None], 'errors')
        assert_types(info, [list, None], 'info')
        super().__init__(request, response_code=response_code)

        if response is None:
            self.response = dict()
        else:
            self.response = deepcopy(response)
        if errors is None:
            self.errors = dict()
        else:
            self.errors = deepcopy(errors)
        if info is None:
            self.info = list()
        else:
            self.info = deepcopy(info)
        self.add_header("Content-Type", "application/json")

    def render(self):
        self.content = json.dumps({"response": self.response, "errors": self.errors, "info": self.info})
        return super().render()


def parse_json_request(request: Request):
    """
    Standartized JSON request parser. A simple way not to confuse myself and a quick way to 'fix' incoming data.
    """
    parsable_json = ""
    if not request.isJSON:
        raise Error400("Request has no 'application/json' header.")
    try:
        # Fix incoming json.
        parsable_json = str(request.request_body)[2: -1]           # Remove surrounding b'...' from b'{json}'
        parsable_json = unquote(parsable_json)                     # Remove URI encoding.
        return json.loads(parsable_json)
    except Exception:   # I don't care
        raise Error400("JSON request cannot be parsed. json string %s " % parsable_json)


def return_400(request):
    """
    Error 400 response shortcut
    """
    print("Got error 400 for path %s" % request.path)
    traceback.print_exc()
    response = Response(request, "ERROR 400: BAD REQUEST >:(", 400)
    return response.render()


def return_404(request):
    """
    Error 404 response shortcut
    """
    print("Got Error 404 for path '%s'" % request.path)
    traceback.print_exc()
    response = Response(request, "ERROR 404: NOT FOUND :<", 404)
    return response.render()


def return_500(request):
    """
    Error 500 response shortcut
    """
    print("Got Error 500 for path '%s'" % request.path)
    traceback.print_exc()
    response = Response(request, "ERROR 500: Internal Server Error :'(", 500)
    return response.render()


def get_template(template_path):
    """
    Good function should use some caching here, instead of reading files for each request... but who cares?
    :param template_path:
    :return:
    """
    with open(template_path, encoding="utf-8") as file:
        template_text = file.read()
    return template_text


def process_template(template_text, variables):
    """
    Oppa Django style!
    Replaces all ocurrences of {{variable}} in template_text by corresponding value from variables dict.
    :return: updated template_text
    """
    result = template_text
    for key, value in variables.items():
        result = result.replace("{{%s}}" % key, value)
    return result


def render_to_response(request, template, params=None):
    """
    Template render shortcut
    :param request:
    :param template: path to html file.
    :param params: optional, dict with variables for replace in template's text.
    :return: Response object
    """
    response = Response(request)
    response.content = get_template(template)
    if params is not None:
        print(params)
        response.content = process_template(response.content, params)
    return response


class RegexpValidator:
    def __init__(self, pattern, error_text):
        self.pattern = re.compile(pattern)
        self.error_text = error_text

    def validate(self, value):
        if self.pattern.match(value) is None:
            return self.error_text
        return 0


def simple_screen(text):
    """
    Truly primitive screening function. Sorry, I'm running out of time to google or code something better.
    :param text:
    :return:
    """
    return text.replace('"', '\\"')


def json_error_status(fn):
    """
    Decorator to catch HtmlError exceptions and wrap them into standartized json reply with corresponding status.
    :return:
    """
    return fn   # TODO
